// index.js
// run with node --experimental-worker index.js on Node.js 10.x
const {
    Worker
} = require('worker_threads')
var sleep = require('system-sleep');
const os = require('os')
const cpuCount = os.cpus().length
const useragent = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/42.0 Safari/537.31',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.10 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.10',
    'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; GTB7.5; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0) LinkCheck by Siteimprove.com',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H143 Safari/600.1.4',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:37.0) Gecko/20100101 Firefox/37.0',
    'Mozilla/5.0 (Windows NT 5.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) QuickLook/5.0',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; MATBJS; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)',
    'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; ASU2JS; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
    'Mozilla/5.0 (Windows NT 6.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 5.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; TNJB; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.2)',
    'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.2.155 Chrome/44.2.2403.155 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
    'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.2; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; Tablet PC 2.0; McAfee; AmericasCardroom; BRI/2; GWX:DOWNLOADED)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/7.0)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2483.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; MS-RTC LM 8)',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:40.0) Gecko/20100101 Firefox/40.0 Cyberfox/40.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; GWX:QUALIFIED; MASMJS)',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; NP06; rv:11.0) like Gecko',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; ARM; Trident/7.0; Touch; .NET4.0E; .NET4.0C; Tablet PC 2.0)',
    'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.0.19) Gecko/2010062819 Firefox/3.0.19 Flock/2.6.1',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25',
    'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
]

const proxy = [
    '173.208.36.31:3128', '108.186.244.8:3128', '173.208.36.69:3128', '104.140.183.120:3128', '173.232.14.214:3128',
    '192.126.158.83:3128', '192.126.158.85:3128', '206.214.82.248:3128', '173.208.36.245:3128', '206.214.82.113:3128',
    '173.208.36.31:3128', '108.186.244.8:3128', '173.208.36.69:3128', '104.140.183.120:3128', '173.232.14.214:3128',
    '192.126.158.83:3128', '192.126.158.85:3128', '206.214.82.248:3128', '173.208.36.245:3128', '206.214.82.113:3128',
    '104.140.73.213:3128', '104.140.73.249:3128', '173.208.36.235:3128', '192.126.158.131:3128', '104.140.183.100:3128',
    '108.186.244.178:3128', '173.234.248.203:3128', '173.234.248.168:3128', '173.232.14.109:3128', '173.208.36.61:3128',
    '173.232.14.243:3128', '192.126.158.150:3128', '192.126.158.123:3128', '192.126.158.63:3128', '192.126.166.167:3128',
    '192.126.162.144:3128', '173.234.248.189:3128', '108.186.244.60:3128', '173.232.14.135:3128', '104.140.183.98:3128',
    '173.208.36.11:3128', '108.186.244.145:3128', '173.232.14.21:3128', '192.126.158.81:3128', '108.186.244.46:3128',
    '104.140.73.44:3128', '192.126.166.100:3128', '104.140.183.230:3128', '192.126.158.64:3128', '104.140.183.178:3128',
    '206.214.82.38:3128', '206.214.82.92:3128', '192.126.166.164:3128', '192.126.166.71:3128', '173.232.14.220:3128',
    '108.186.244.83:3128', '173.234.248.83:3128', '192.126.158.253:3128', '104.140.73.150:3128', '206.214.82.31:3128',
];

const no = ['82124636160', '87784083690', '83146427708', '83146427717', '83146469676', '83146427901', '83146469677', '83146427896', '83856925344', '83856925323',
    '83146427895', '83856925326', '83146427883', '83856925328', '83146427863', '83856925297', '83856925338', '83856925329', '83146427861', '83146427862',
    '83856925300', '83856925301', '83856925317', '83856925315', '83146427726', '83133629500', '83856925335', '83856925309', '83133629498', '83856925311',
    '83133629499', '83856925316', '83856925435', '83856925449', '83133629494', '83856925304', '83146427746', '83856925306', '83856925439', '83856925438',
    '83856925302', '83146427724', '83856925303', '83146427723', '83856925442', '83856925319', '83146427860', '83146427859', '83856925444', '83856925314',
    '83856925321', '83146427790', '83856925308', '83856925446', '83856925445', '83856925312', '83146427793', '83146469668', '83856925307', '83856925345',
    '83146427783', '83146427789', '83856925354', '83856925339', '83146469638', '83146469642', '83146427782', '83856925342', '83856925343', '83146427780',
    '83146469641', '83146469640', '83856925341', '83856925327', '83146469639', '83146429627', '83856925313', '83856925310', '83146469644', '83146469664',
    '83856925351', '83856925353', '83146469654', '83146469655', '83856925331', '83856925330', '83146469656', '83856925299', '83146469657', '83856925298',
    '83856925337', '83146469666', '83146469660', '83856925336', '83146469659', '83146469658', '83856925332', '83856925334', '83856925322', '83146469663',
    '83856925324', '83146469662', '83856925349', '83856925346', '83146469646', '83146469637', '83133629506', '83146469670', '83146469675', '83146469645',
    '83856926067', '83856926068', '83146469649', '83146469650', '83146469683', '83146469681', '83146469679', '83146469680', '83146469687', '83146469661',
    '83146469685', '83146427921', '83146469682', '83856926083', '83856926098', '83146469684', '83146469653', '83146469648', '83146469647', '83856926084',
    '83856926093', '83146469651', '83146469652', '83146427719', '83856926114', '83856926097', '83146427840', '83146427834', '83146469673', '83146427837',
    '83146469674', '83146469631', '83146427825', '83146427828', '83856926071', '83146427823', '83146469628', '83856926099', '83146469629', '83146427758',
    '83856926110', '83856926100', '83146469626', '83146469627', '83146427756', '83856926112', '83856926102', '83146427747', '83146469624', '83146469625',
    '83856926087', '83856926065', '83146469633', '83856926117', '83856926113', '83146469632', '83856926115', '83856926061', '83146469634', '83856926062',
    '83856926063', '83146469613', '83146469612', '83146469422', '83146469614', '83146469420', '83146469615', '83146427856', '83146469616', '83146469617',
    '83146469406', '83146469402', '83146427765', '83146469419', '83146469636', '83146427842', '83146469423', '83146469599', '83146427916', '83146469598',
    '83146469425', '83146469424', '83146469597', '83146427908', '83146427914', '83146469596', '83856926090', '83146469594', '83856926092', '83146427905',
    '83146469593', '83146427803', '83146469592', '83856926094', '83146469591', '83856926089', '83856926073', '83146469618', '83146469619', '83146469672',
    '83856926096', '83856926086', '83146469620', '83146469621', '83146469671', '83856926088', '83856926085', '83146469609', '83146469623', '83136517998',
    '83856926106', '83146469610', '83146469600', '83856926103', '83856926104', '83146469601', '83146469602', '83856926105', '83856926107', '83856926108',
    '83146469588', '83146469589', '83856926069', '83146469611', '83146469587', '83146469608', '83146469607', '83146429621', '83146469606', '83146469605',
    '83146469604', '83146469603', '83146429622', '83146468955', '83146468976', '83146427243', '83146427245', '83146468970', '83146429598', '83146429599',
    '83146468975', '83146427238', '83146468994', '83146429595', '83146468951', '83146427237', '83146427236', '83146429638', '83146427235', '83146427305',
    '83146429660', '83146468974', '83146468973', '83146429661', '83146468949', '83146468993', '83146468981', '83146468968', '83146429657', '83146468966',
    '83146468977', '83146429659', '83146468978', '83146468979', '83146429656', '83146468965', '83146468961', '83146429655', '83146468959', '83146468958',
    '83146429640', '83146468954', '83146429651', '83146468972', '83146468971', '83146429596', '83146468964', '83146468962', '83146429649', '83146469013',
    '83146469036', '83146469019', '83146427254', '83146429608', '83146429611', '83146469017', '83146469023', '83146427293', '83146469001', '83146427292',
    '83146469006', '83146429605', '83146469011', '83146429606', '83146427285', '83146427274', '83146429600', '83146427294', '83146469050', '83146427269',
    '83146469147', '83146469064', '83146427295', '83146427147', '83146469069', '83146469024', '83146469026', '83146469002', '83146469144', '83146469678',
    '83146469148', '83146429648', '83146429650', '83146469146', '83146469140', 
]
var num = 0;

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function runService(workerData,resourceLimits) {
    
        return new Promise((resolve, reject) => {
            try{
            const worker = new Worker('./worker.js', {
                workerData,
                resourceLimits
            });
            worker.on('message', resolve);
            worker.on('error', reject);
            worker.on('exit', (code) => {
                if (code !== 0)
                    reject(new Error(`Worker stopped with exit code ${code}`));
            })
        }catch(e){
            console.log(e);
        }})
   
    
}

async function run() {
    // console.log(cpuCount)
    try{
        for (let k = 0; k < proxy.length; k++) {
            for (let i = 0; i < 5; i++) {
                runService({
                    useragent: useragent[getRandomIntInclusive(0, 99)],
                    proxy: proxy[k],
                    phonenum: no[num],
                    link: 'https://www.nimo.tv/live/303589'
                },{maxOldGenerationSizeMb:9999,maxYoungGenerationSizeMb:9999,codeRangeSizeMb:9999,stackSizeMb:9999})
                num++;
            }
           // sleep(5000); // 5 seconds
        }
    
    }catch(e){
        console.log(e);
    }
}

try{
    run().catch(err => console.error(err))
}catch(e){}
