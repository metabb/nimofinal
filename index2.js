// index.js
// run with node --experimental-worker index.js on Node.js 10.x
const {
    Worker
} = require('worker_threads')
var sleep = require('system-sleep');
const os = require('os')
const cpuCount = os.cpus().length
const useragent = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/42.0 Safari/537.31',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.10 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.10',
    'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; GTB7.5; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0) LinkCheck by Siteimprove.com',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H143 Safari/600.1.4',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:37.0) Gecko/20100101 Firefox/37.0',
    'Mozilla/5.0 (Windows NT 5.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) QuickLook/5.0',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; MATBJS; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)',
    'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; ASU2JS; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
    'Mozilla/5.0 (Windows NT 6.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 5.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; TNJB; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.2)',
    'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.2.155 Chrome/44.2.2403.155 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
    'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.2; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; Tablet PC 2.0; McAfee; AmericasCardroom; BRI/2; GWX:DOWNLOADED)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/7.0)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2483.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; MS-RTC LM 8)',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:40.0) Gecko/20100101 Firefox/40.0 Cyberfox/40.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; GWX:QUALIFIED; MASMJS)',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; NP06; rv:11.0) like Gecko',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; ARM; Trident/7.0; Touch; .NET4.0E; .NET4.0C; Tablet PC 2.0)',
    'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.0.19) Gecko/2010062819 Firefox/3.0.19 Flock/2.6.1',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25',
    'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
]

const proxy = [
    '104.140.73.44:3128', '192.126.166.100:3128', '104.140.183.230:3128', '192.126.158.64:3128', '104.140.183.178:3128',
    '206.214.82.38:3128', '206.214.82.92:3128', '192.126.166.164:3128', '192.126.166.71:3128', '173.232.14.220:3128',
    '108.186.244.83:3128', '173.234.248.83:3128', '192.126.158.253:3128', '104.140.73.150:3128', '206.214.82.31:3128',
    '173.232.14.5:3128', '192.126.162.119:3128', '173.234.248.37:3128', '104.140.183.39:3128', '104.140.73.45:3128',
    '192.126.162.191:3128', '104.140.183.13:3128', '192.126.162.102:3128', '206.214.82.242:3128', '108.186.244.23:3128',
    '173.208.36.64:3128', '108.186.244.237:3128', '192.126.166.52:3128', '173.208.36.49:3128', '173.232.14.197:3128',
    '173.234.248.54:3128', '192.126.162.11:3128', '206.214.82.252:3128', '192.126.166.102:3128', '173.234.248.24:3128',
    '173.234.248.213:3128', '206.214.82.112:3128', '192.126.162.224:3128', '192.126.166.101:3128', '192.126.162.189:3128',
    '104.140.73.208:3128', '173.208.36.38:3128', '192.126.166.161:3128', '173.234.248.5:3128', '206.214.82.155:3128',
    '173.232.14.80:3128', '104.140.73.165:3128', '192.126.166.127:3128', '104.140.73.226:3128', '104.140.73.135:3128',
    '192.126.154.89:3128', '23.19.32.233:3128', '206.214.82.225:3128', '23.19.32.122:3128', '170.130.66.66:3128',
    '170.130.66.128:3128', '173.234.225.210:3128', '206.214.82.47:3128', '23.19.32.116:3128', '104.140.210.56:3128',
];

const no = [ '83146429646', '83146429647', '83146427519', '83146427518', '83146429644',
    '83146427521', '83146427520', '83146429645', '83146427522', '83146427528', '83146429642', '83146429643', '83146427229', '83146427227', '83146429619',
    '83146427510', '83146429618', '83146469669', '83146427142', '83146427306', '83146429617', '83146427500', '83146429613', '83146427307', '83146427313',
    '83146427326', '83146427394', '83146427314', '83146427372', '83146427320', '83146427365', '83146429603', '83146427377', '83146427338', '83146427322',
    '83146427321', '83146429633', '83146427324', '83146429632', '83146427362', '83146427350', '83146427364', '83146427382', '83146427225', '83146427224',
    '83146429631', '83146427490', '83146427223', '83146427221', '83146427491', '83146427403', '83146429653', '83146427499', '83146429654', '83146427215',
    '83146427486', '83146427496', '83146429652', '83146427156', '83146429597', '83146427232', '83146427477', '83146427480', '83146427145', '83146427268',
    '83146427385', '83146427380', '83146427424', '83146429626', '83146427265', '83146427493', '83146427264', '83146429625', '83146427407', '83146427471',
    '83146427263', '83146427260', '83146429623', '83146427413', '83146427296', '83146427308', '83146429624', '83146427461', '83146427488', '83146427255',
    '83146427258', '83146427464', '83146427514', '83146427251', '83146427143', '83146427459', '83146469665', '83133016491', '83146427426', '83146427431',
    '83146427467', '83146427442', '83133016484', '83133016486', '83146427474', '83146427436', '83133016480', '83133016482', '83133016173', '83133016470',
    '83133016476', '83133016459', '83133016171', '83133016457', '83133016172', '83133016169', '83133016167', '83133016170', '83133016166', '83133016168',
    '83133016165', '83133016187', '83133016186', '83133016183', '83133016184', '83133016181', '83133016182', '83133016176', '83133016135', '83133016175',
    '83133016180', '83133016179', '83133016134', '83133016192', '83133016191', '83133016190', '83133016189', '83133016178', '83133016188', '83133016136',
    '83133016137', '83133016164', '83133016138', '83133016139', '83133016140', '83133016146', '83133016141', '83133016145', '83133016144', '83133016147',
    '83133016142', '83133016143', '83133016148', '83133016150', '83133016152', '83133016149', '83133016161', '83133016154', '83133016157', '83146427455',
    '83133016177', '83856925083', '83856925087', '83856925086', '83856925061', '83856925062', '83856925085', '83856925073', '83856925074', '83856925063',
    '83856925058', '83856925072', '83856925071', '83856925082', '83856925081', '83856925080', '83856925079', '83856925075', '83856925076', '83856925065',
    '83856925066', '83856925070', '83856925068', '83856925089', '83856925099', '83856925114', '83856925094', '83856925060', '83856925059', '83856925064',
    '83856925088', '83856925098', '83856925097', '83856925091', '83856925092', '83856925057', '83856925093', '83856925095', '83856925056', '83856925096',
    '83856925111', '83856925112', '83856925101', '83856925100', '83856925105', '83856925102', '83856925106', '83856925104', '83856925107', '83856925109',
    '83856925108', '83146427513', '83133016174', '83133016185', '83856925320', '83146468952', '83856925440', '83146468969', '83856926109', '83856925443',
    '83146429634', '83856925325', '83146427796', '83146469635', '83146469590', '83146427240', '83146427304', '83146469630', '83146469667', '83133016489',
    '83133016488', '83133016597', '83133016595', '83133016598', '83133016599', '083146427220', '83133016677', '83133016613', '83133016643', '83133016658',
    '83133016645', '83133016662', '83133016661', '83133016663', '83133016634', '83133016679', '83133016667', '83133016671', '83133016678', '83133016668',
    '83133016602', '83133016600', '83133016655', '83133016652', '83133016640', '83133016638', '83133016637', '83133016670', '83133016633', '83133016630',
    '83133016629', '83133016620', '83133016669', '83133016648', '83133016619', '83133016623', '83133016617', '83133016618', '83133016616', '83133016609',
    '83133016606', '83133016624', '83133016675', '83133016603', '83133016674', '83133016627', '83133016615', '83133016676', '83133016673', '83133016680',
]
var num = 0;

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function runService(workerData,resourceLimits) {
    
        return new Promise((resolve, reject) => {
            try{
            const worker = new Worker('./worker.js', {
                workerData,
                resourceLimits
            });
            worker.on('message', resolve);
            worker.on('error', reject);
            worker.on('exit', (code) => {
                if (code !== 0)
                    reject(new Error(`Worker stopped with exit code ${code}`));
            })
        }catch(e){
            console.log(e);
        }})
   
    
}

async function run() {
    // console.log(cpuCount)
    try{
        for (let k = 0; k < proxy.length; k++) {
            for (let i = 0; i < 5; i++) {
                runService({
                    useragent: useragent[getRandomIntInclusive(0, 99)],
                    proxy: proxy[k],
                    phonenum: no[num],
                    link: 'https://www.nimo.tv/live/303589'
                },{maxOldGenerationSizeMb:9999,maxYoungGenerationSizeMb:9999,codeRangeSizeMb:9999,stackSizeMb:9999})
                num++;
            }
           // sleep(5000); // 5 seconds
        }
    
    }catch(e){
        console.log(e);
    }
}

try{
    run().catch(err => console.error(err))
}catch(e){}
