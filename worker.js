const puppeteer = require('puppeteer');

var click_number = '#container > div > div.nimo-login-content-wrapper.n-fx-col.n-fx-ss > div.nimo-login-body.n-fx-sn.n-fx1.n-fx-basis-auto.n-as-w100 > div.nimo-login-body-first.n-fx1.n-fx-basis-auto > div.nimo-login-body-input.n-fx-sn.bc10.n-as-rnd.n-as-mrgb-md.n-as-rel.n-as-of-hidden > div.nimo-login-body-area-code.c1.n-as-padh.n-fx-sr0.line-height40.n-as-pointer';
var indo = '#container > div > div:nth-child(2) > div > div > div > div.n-as-scroll.n-fx1.n-as-of-auto > div:nth-child(1)';
var number = '#container > div > div.nimo-login-content-wrapper.n-fx-col.n-fx-ss > div.nimo-login-body.n-fx-sn.n-fx1.n-fx-basis-auto.n-as-w100 > div.nimo-login-body-first.n-fx1.n-fx-basis-auto > div.nimo-login-body-input.n-fx-sn.bc10.n-as-rnd.n-as-mrgb-md.n-as-rel.n-as-of-hidden > input';
var password = '#container > div > div.nimo-login-content-wrapper.n-fx-col.n-fx-ss > div.nimo-login-body.n-fx-sn.n-fx1.n-fx-basis-auto.n-as-w100 > div.nimo-login-body-first.n-fx1.n-fx-basis-auto > div.nimo-login-body-input.n-fx-sn.bc10.n-as-rnd.n-as-mrgb.n-as-rel.n-as-of-hidden > input';
var login = '#container > div > div.nimo-login-content-wrapper.n-fx-col.n-fx-ss > div.nimo-login-body.n-fx-sn.n-fx1.n-fx-basis-auto.n-as-w100 > div.nimo-login-body-first.n-fx1.n-fx-basis-auto > button';
var search = '#header > div > div.nimo-header-main-menu.n-fx0.n-as-h60px.n-fx-bc > div.n-as-mrgh.c-hover1 > ul > a:nth-child(1) > li';
var box = '#container > div > div:nth-child(2) > div > div > div > div.bc4.search-input.n-fx-sc.n-as-mrgb.n-fx-sr0.n-as-mrgh > input';
const { workerData: { useragent, proxy, phonenum,link } } = require('worker_threads');




  
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
  }

console.log('Process triggered.');
console.time('Execution time');

/**
 * @return {Promise}
 */
const sleep = async function () {
    return new Promise(resolve => {
        setTimeout(resolve, Math.floor(Math.random() * 5000) + 999);
    });
};
var count=0;

(async () => {
    try{
        process.setMaxListeners(999999);
       // process.removeAllListeners();
          
             const browser = await puppeteer.launch({
               headless: true,
               args: [`--proxy-server=${proxy}`,
               '--no-sandbox',
         '--disable-setuid-sandbox',
         '--disable-dev-shm-usage',
         '--disable-accelerated-2d-canvas',
         '--no-first-run',
         '--no-zygote',
         '--single-process', // <- this one doesn't works in Windows
         '--disable-gpu',
               '--incognito',
           ]
             });
             // const context = await browser.createIncognitoBrowserContext();
             const getPages = await browser.pages();
             const page = getPages[0];
            
             
             // await page.setRequestInterception(true);
             
             console.log(proxy + '  '+ phonenum);
             
             
             await page.setViewport({
               width: getRandomIntInclusive(0, 2040),
               height: getRandomIntInclusive(0, 2040),
               deviceScaleFactor: 1,
             });
             
           await page.authenticate({
                 username: 'nich_1310@yahoo.com',
                 password: 'N!ch0l@5'
             });
           // await page.waitFor(2000);
          await page.setUserAgent(useragent);
          await page.goto('https://www.nimo.tv/login', {
           waitLoad: true, 
           timeout: 240000,
           waitNetworkIdle: true // defaults to false
           });
               await page.waitForSelector(click_number);
               await page.click(click_number);
               await page.waitForSelector(box);
               await page.type(box,'Indonesia');
               await page.click(indo);
               await page.type(number,phonenum);
              await page.type(password,'alen04');
              
               await Promise.all([
                 page.click(login),
                 page.waitForNavigation({ 
                   waitLoad: true, 
                   timeout: 240000,
                   waitNetworkIdle: true // defaults to false
                 }),
           ]);
           await page.setRequestInterception(true)
           await page.on('request', request => {
               if (request.resourceType() === 'image'|| request.resourceType () === 'media' || request.resourceType () === 'font' || request.resourceType () === 'stylesheet')
                 request.abort();
               else
                 request.continue();
               });
               await page.goto('https://www.nimo.tv/',{
                   waitLoad: true, 
                   timeout: 240000,
                   waitNetworkIdle: true // defaults to false
               });
               console.log('landing page success');
               await page.goto(link,{
                   waitLoad: true, 
                   timeout: 240000,
                   waitNetworkIdle: true // defaults to false
               });
              //browser.disconnect();
              process.removeAllListeners();
              console.log('Process end.');
              console.timeEnd('Execution time');

       }catch(e){}
   
})();
