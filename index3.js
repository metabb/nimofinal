// index.js
// run with node --experimental-worker index.js on Node.js 10.x
const { Worker } = require('worker_threads')
var sleep = require('system-sleep');
const os = require('os')
const cpuCount = os.cpus().length
const useragent = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12',
  'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9',
  'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
  'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
  'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
  'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
  'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/42.0 Safari/537.31',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
  'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.10 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.10',
  'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
  'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
  'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
  'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
  'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; GTB7.5; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)',
  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0) LinkCheck by Siteimprove.com',
  'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.3; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (iPhone; CPU iPhone OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H143 Safari/600.1.4',
  'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
  'Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0',
  'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:37.0) Gecko/20100101 Firefox/37.0',
  'Mozilla/5.0 (Windows NT 5.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) QuickLook/5.0',
  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; MATBJS; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)',
  'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0',
  'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; ASU2JS; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
  'Mozilla/5.0 (Windows NT 6.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 5.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; TNJB; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0',
  'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.2)',
  'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
  'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.2.155 Chrome/44.2.2403.155 Safari/537.36',
  'Mozilla/5.0 (X11; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2',
  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
  'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.2; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; Tablet PC 2.0; McAfee; AmericasCardroom; BRI/2; GWX:DOWNLOADED)',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
  'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/7.0)',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2483.0 Safari/537.36',
  'Mozilla/5.0 (Windows NT 5.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; MS-RTC LM 8)',
  'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:40.0) Gecko/20100101 Firefox/40.0 Cyberfox/40.0',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; GWX:QUALIFIED; MASMJS)',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; NP06; rv:11.0) like Gecko',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; ARM; Trident/7.0; Touch; .NET4.0E; .NET4.0C; Tablet PC 2.0)',
  'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.0.19) Gecko/2010062819 Firefox/3.0.19 Flock/2.6.1',
  'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25',
  'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
  ]
  
  const proxy = [
    '192.126.162.167:3128','192.126.154.36:3128','192.126.158.221:3128','173.208.43.93:3128','173.234.225.22:3128',
'104.140.210.89:3128','206.214.82.16:3128','173.234.248.145:3128','192.126.158.127:3128','173.234.225.15:3128',
'170.130.66.107:3128','23.19.32.228:3128','173.208.43.61:3128','173.234.225.47:3128','23.19.32.50:3128',
'104.140.183.104:3128','192.126.154.125:3128','104.140.183.173:3128','23.19.32.177:3128','173.208.43.143:3128',
'192.126.158.229:3128','173.234.248.75:3128','192.126.162.90:3128','170.130.59.144:3128','206.214.82.94:3128',
'173.234.225.192:3128','173.234.248.170:3128','104.140.183.152:3128','23.19.32.4:3128','104.140.210.190:3128',
'206.214.82.241:3128','192.126.154.6:3128','192.126.158.108:3128','173.234.248.61:3128','104.140.183.153:3128',
'104.140.210.250:3128','104.140.210.184:3128','104.140.210.251:3128','170.130.59.46:3128','170.130.59.146:3128',
'173.208.43.241:3128','104.140.210.59:3128','104.140.210.231:3128','192.126.154.222:3128','104.140.183.238:3128',
'170.130.59.15:3128','173.234.248.190:3128','173.234.225.115:3128','192.126.154.191:3128','170.130.66.91:3128',
'104.140.210.150:3128','170.130.66.173:3128','23.19.32.29:3128','192.126.162.177:3128','104.140.183.102:3128',
'192.126.158.232:3128','192.126.158.143:3128','170.130.66.222:3128','173.234.248.39:3128','173.208.43.73:3128',
  ];
  
  const no = ['83856925801','83856925803','83856925800','83856925802','83856925798','83856925799','83856925796','83856925794','83856925797','83856925795',
  '83856925782','83856925814','83856925819','83856925785','83856925781','83856925775','83856925776','83856925784','83856925807','83856925822',
  '83856925821','83856925792','83856925793','83856925791','83856925815','83856925816','83856925826','83856925787','83856925774','83856925780',
  '83856925825','83856925820','83856925805','83856925779','83856925808','83856925806','83856925788','83856925783','83856925789','83856925810',
  '83856925786','83856925817','83856925818','83856925809','83856925804','83856925824','83856925813','83856925790','83856925811','83133629504',
  '83133629502','83133629501','83133016622','83146427763','83146428437','83146428436','83146429556','83146428434','83146429547','83146429552',
  '83146429544','83146429543','83146429471','83146429404','83146429467','83146429023','83146429014','83146428695','83146428456','83146428463',
  '83146428438','83146428439','83146461251','83146429663','83146429592','83146429593','83146429665','83146429666','83146429423','83146429424',
  '83146429669','83146429667','83146429436','83146429437','83146429558','83146429672','83146429559','83146429670','83146429560','83146429562',
  '83146429673','83146429566','83146429674','83146429568','83146461526','83146429569','83146429571','83146429675','83146462183','83146429570',
  '83146429563','83146462445','83146462467','83146429444','83146463853','83146463441','83146464356','83146429447','83146429410','83146464377',
  '83146467249','83146429460','83146429457','83146429662','83146467246','83146467248','83146429456','83146467237','83146465796','83146465802',
  '83146429574','83146429579','83146466600','83146429582','83146429580','83146466661','83146466672','83146429586','83146467236','83146429583',
  '83146466686','83146467238','83146469582','83146469559','83146467240','83146467234','83146469581','83146469585','83146467241','83146467242',
  '83146469549','83146467243','83146467244','83146466704','83146466705','83146467245','83146467247','83146467231','83146469550','83146467230',
  '83146467233','83146465795','83146461023','83146467232','83146429676','83146469562','83146469564','83136390879','83146469563','83136390878',
  '83136390877','83146469565','83136390876','83136390875','83136390897','83136390896','83136390894','83136390892','83136390893','83136390889',
  '83136390883','83136390868','83136390867','83136390866','83136390865','83136390864','83136390862','83136390861','83136390882','83136390890',
  '83136390891','83136390855','83136390903','83136390902','83136390854','83136390901','83136390900','83136390874','83136390898','83136390885',
  '83136390872','83136390871','83136390870','83136390880','83136390899','83136390857','83136390858','83136390884','83136390863','83136390860',
  '83136390859','83136390856','83136390888','83136390887','83136390895','83136390869','83136390873','83136390881','83146469567','83146469558',
  '83146469557','83146469556','83146469554','83146469553','83146469552','83146469551','83146469586','83146469584','83146469583','83146469569',
  '83146469571','83146469573','83146469572','83146469575','83146469536','83146469540','83146469537','83146469538','83146469539','83146469544',
  '83146469547','83146469543','83146469541','83146469542','83146469546','83146469545','83133016527','83133016531','83133016540','83133016539',
  '83133016542','83133016543','83133016548','83133016544','83133016545','83133016524','83133016522','83133016546','83133016519','83133016520',
  '83133016521','83133016518','83133016516','83133016514','83133016515','83133016512','83133016594','83133016579','83133016593','83133016577',
  '83133016578','83133016576','83133016574','83146429546','83146428468','83146429453','83146429442','83146469548','83146469560','83146469555',
  '83146469566','83146469568','83146469561','83133016565','83133016572','83133016591','83133016564','83133016590','83133016587','83133016569',
  '83133016566','83133016585','83133016580','83133016581','83133016584','83133016549','83133016551','83133016550','83133016552','83133016553',
  ]
  var num=0;
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
  }
  function runService(workerData,resourceLimits) {
    
    return new Promise((resolve, reject) => {
        try{
        const worker = new Worker('./worker.js', {
            workerData,
            resourceLimits
        });
        worker.on('message', resolve);
        worker.on('error', reject);
        worker.on('exit', (code) => {
            if (code !== 0)
                reject(new Error(`Worker stopped with exit code ${code}`));
        })
    }catch(e){
        console.log(e);
    }})


}

async function run() {
    // console.log(cpuCount)
    try{
        for (let k = 0; k < proxy.length; k++) {
            for (let i = 0; i < 5; i++) {
                runService({
                    useragent: useragent[getRandomIntInclusive(0, 99)],
                    proxy: proxy[k],
                    phonenum: no[num],
                    link: 'https://www.nimo.tv/live/303589'
                },{maxOldGenerationSizeMb:9999,maxYoungGenerationSizeMb:9999,codeRangeSizeMb:9999,stackSizeMb:9999})
                num++;
            }
           // sleep(5000); // 5 seconds
        }
    
    }catch(e){
        console.log(e);
    }
}

try{
    run().catch(err => console.error(err))
}catch(e){}
