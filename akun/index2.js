// index.js
// run with node --experimental-worker index.js on Node.js 10.x
const {
    Worker
} = require('worker_threads')
var sleep = require('system-sleep');
const uagent = require('./useragent.js');
const proxy = require('./proxy.js');
const http = require('http');
var data = [];
var qs = require('querystring');
var fs = require('fs')
var ini = require('ini');
const { exec } = require("child_process");
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'))

var num = 0;

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function runService(workerData,resourceLimits) {
    
    return new Promise((resolve, reject) => {
        try{
        const worker = new Worker('./worker.js', {
            workerData,
            resourceLimits
        });
        worker.on('message', resolve);
        worker.on('error', reject);
        worker.on('exit', (code) => {
            if (code !== 0)
                reject(new Error(`Worker stopped with exit code ${code}`));
        })
    }catch(e){
        console.log(e);
    }})


}

async function run(link) {
// console.log(cpuCount)

    try{
        for (let j=0;j <2;j++){
        for (let k = 200; k < 399; k++) {
            var config2 = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
           // const words = no[num].split(':');
            if(config2.status.status === "on"){
                for (let i = 0; i < 1; i++) {
                    runService({
                        useragent: uagent.useragent[getRandomIntInclusive(0, 99)],
                        proxy: proxy.proxy[k],
                        link: `https://www.nimo.tv/${link}`
                    },{maxOldGenerationSizeMb:128,maxYoungGenerationSizeMb:64,codeRangeSizeMb:64,stackSizeMb:64})
                    num++;
                    console.log(num + ' ' + proxy[k]);
                  sleep(10000);
                }
            }else if(config2.status.status === "off"){
                num=0;
                break;
            }
            sleep(5000);
        }
        }
    }catch(e){
        console.log(e);
    }
   


}


const requestListener = (req, res) => {
switch (req.url) {
    case '/':
      switch(req.method){
        case 'GET':
            tampilkanForm(res);
            break;
        case 'POST':
            prosesData(req, res);
            break;
        default:
            badRequest(res);
    }
        break;
    default:
        res.writeHead(404).end('NOK');
        break;
}

};

function tampilkanForm(res){
  if(config.status.status === "on"){
    var html = '<html><head><title>Nimo Start </title></head><body>'
    + '<h1>URL Nimo : </h1>' + config.link.user
    + '<form method="post" action="/">'
    + `<p><input type="text" name="url" value"${config.link.user} disabled="true"></p>`
    + '<p><input type="submit" value="Stop"></p>'
    + '</form></body></html>';

//    res.setHeader('Content-Type', 'text/html');
// res.setHeader('Content-Length', Buffer.byteLength(html));

  }else if(config.status.status === "off"){
    var html = '<html><head><title>Nimo Start </title></head><body>'
    + '<h1>Masukan URL Nimo : </h1>'
    + '<form method="post" action="/">'
    + '<p><input type="text" name="url"></p>'
    + '<p><input type="submit" value="Start"></p>'
    + '</form></body></html>';
  //  return res.redirect('/');

}
res.end(html);
}

function prosesData(req, res) {
  if(config.status.status === "off"){
    var body= '';
    req.setEncoding('utf-8');
    req.on('data', function(chunk){
        body += chunk;
        var data = qs.parse(body);
        config.link.user = data.url;
        config.status.status ='on';
        fs.writeFileSync('./config.ini', ini.stringify(config));
       
    });
   
  }
  if(config.status.status === "on"){
      console.log('asd')
    config.status.status ='off';
    fs.writeFileSync('./config.ini', ini.stringify(config));
    console.log('success write ini');
    exec("pkill -f chrome", (error, stdout, stderr) => {
        console.log('success write ini2');
        return;
    });
    //exit=true;
   
  }



req.on('end', function(){
   console.log('a');
   if(config.status.status === "on"){
    var data = qs.parse(body);
  //  res.setHeader('Content-Type', 'text/plain');
    res.end('URL Nimo : '+ config.link.user + '  Status :  ' + config.status.status);
   try{
    run(config.link.user).catch(err => console.error(err))
    }catch(e){}
   }
   if(config.status.status === "off"){
   // res.setHeader('Content-Type', 'text/plain');
    res.end('URL Nimo : '+ config.link.user + '  Status :  ' + config.status.status);
   }
   
   
});

}

function badRequest(res){
res.statusCode = 400;
res.setHeader('Content-Type', 'text/plain');
res.end('400 - Bad Request');
}

function notFound(res) {
res.statusCode = 404;
res.setHeader('Content-Type', 'text/plain');
res.end('404 - Not Found');
}

const server = http.createServer(requestListener);

server.listen(3333, () => {
console.log('Server listening on: 127.0.0.1:3333');
});

